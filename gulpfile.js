const gulp = require('gulp');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const eslint = require('gulp-eslint');
const sass = require('gulp-sass');
const inject = require('gulp-inject');
const clean = require('gulp-rimraf');
const bowerFiles = require('main-bower-files');
const pkg = require('./package.json');
const bower = require('./bower.json');
const flatten = require('gulp-flatten');
const express = require('express');
const app = express();
const proxy = require('express-http-proxy');
const path = require('path');
const _ = require('lodash');

const dest = '../../webapp/'+pkg.name;
const src = 'src';

gulp.task('clean', () => {
  return gulp.src(dest+'/*' , {read: false})
    .pipe(clean({force: true}));
});

gulp.task('assets', () => {
  return gulp.src([
    src+'/assets/**/*',
    '!'+src+'/assets/stylesheets/*'
  ])
    .pipe(gulp.dest(dest+'/assets'));
});

gulp.task('html', () => {
  return gulp.src(src+'/app/**/*.html')
    .pipe(flatten())
    .pipe(gulp.dest(dest+'/partials'));
});

gulp.task('css', () => {
  return gulp.src(src+'/assets/stylesheets/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('app.css'))
    .pipe(gulp.dest(dest+'/assets/stylesheets'));
});

gulp.task('lint', () => {
  return gulp.src(src+'/app/**/*.js')
    .pipe(eslint({fix: true}))
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
})

gulp.task('app', ['lint'], () => {
  return gulp.src(src+'/app/**/*.js')
    .pipe(babel({presets: ['es2015']}))
    .pipe(gulp.dest(dest+'/app'));
});

gulp.task('vendor', () => {
  return gulp.src(bowerFiles())
  .pipe(gulp.dest(dest+'/vendor'));
});

gulp.task('js', ['app', 'vendor']);

gulp.task('inject', ['css', 'js'], () => {

  _.defaults(bower, {priority: {}, ignored: []});

  let vendor = bowerFiles()
    .map((p) => path.basename(p))
    .sort((a, b) => {
      a = bower.priority.hasOwnProperty(a) ? bower.priority[a] : 0;
      b = bower.priority.hasOwnProperty(b) ? bower.priority[b] : 0;
      return a-b;
    })
    .filter((p) => bower.ignored.indexOf(p) < 0)
    .map((p) => dest+'/vendor/' + p);

  return gulp.src(src+'/index.html')
    .pipe(inject(gulp.src([
      dest+'/app/**/*.js',
      dest+'/assets/stylesheets/**/*.css'
    ], {cwd: __dirname + '/' + dest}), {name: 'app', addPrefix: '/gsm/'+pkg.name}))
    .pipe(inject(gulp.src(
      vendor,
      {cwd: __dirname + '/' + dest}), {name: 'vendor', addPrefix: '/gsm/'+pkg.name}
    ))
    .pipe(gulp.dest(dest));
});

gulp.task('build', ['clean'], () => {
  return gulp.start(['assets', 'html', 'inject']);
});

gulp.task('connect', () => {
  app.use('/gsm/'+pkg.name, express.static('./../../webapp/'+pkg.name));
  app.use('/gsm/rest', proxy('http://localhost:8080', {
    forwardPath: (req, res) => {
      return '/gsm/rest'+req.url;
    }
  }));
  app.listen(3000, () => console.log('running 3000'));
});

gulp.task('watch', ['build', 'connect'], () => {
  gulp.watch(src+'/app/**/*.js', ['js']);
  gulp.watch(src+'/assets/stylesheets/**/*.scss', ['css']);
  gulp.watch(src+'/app/**/*.html', ['html']);
});

gulp.task('default', ['build']);
